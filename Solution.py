import copy


class Solution:
    def __init__(self, d_matrix, f_matrix, bijection_map):
        self.__d_matrix = d_matrix
        self.__f_matrix = f_matrix
        self.__bijection_map = bijection_map

    def get_copy(self):
        solution = copy.copy(self)
        solution.set_bijection_map(self.get_bijection_map())
        return solution

    def get_bijection_map(self):
        return copy.copy(self.__bijection_map)

    def set_bijection_map(self, bijection_map):
        self.__bijection_map = bijection_map

    def get_cost(self):
        cost_sum = 0
        dimension = self.__d_matrix.get_max_row_index() + 1
        for i in range(0, dimension):
            for j in range(i + 1, dimension):
                cost_sum += self.__d_matrix.get_value_by_coordinate(i, j) * self.__f_matrix.get_value_by_coordinate(
                    self.__bijection_map[i], self.__bijection_map[j])

        return cost_sum * 2  # whole matrix, not one half only

    def get_cost_with_penalty(self, p_matrix, lambda_weight):
        cost_sum = 0
        dimension = self.__d_matrix.get_max_row_index() + 1
        for i in range(0, dimension):
            for j in range(i + 1, dimension):
                cost_sum += self.__d_matrix.get_value_by_coordinate(i, j) * self.__f_matrix.get_value_by_coordinate(
                    self.__bijection_map[i], self.__bijection_map[j])
            cost_sum += lambda_weight * p_matrix.get_value_by_coordinate(i, self.__bijection_map[i])

        return cost_sum * 2  # whole matrix, not one half only

    def print_solution(self):
        for key, value in enumerate(self.__bijection_map):
            print(str(key) + " => " + str(value), end=', ')
        print("\n" + str(self.get_cost()))

    def save_solution(self, filename):
        with open(filename, "w") as solution:
            solution.write(' '.join(str(i + 1) for i in self.__bijection_map))

    def swap(self, i, j):
        current_solution = self.get_copy()
        bijection_map = current_solution.get_bijection_map()
        temp = bijection_map[i]
        bijection_map[i] = bijection_map[j]
        bijection_map[j] = temp
        current_solution.set_bijection_map(bijection_map)
        return current_solution
