import time

from BaseLocalSearch import BaseLocalSearch
from Solution import Solution
from Matrix import Matrix
from Coordinate import Coordinate


class GuidedLocalSearch (BaseLocalSearch):

    lambda_weight = 100000

    @classmethod
    def search(cls, d_matrix, f_matrix):
        current_solution = BaseLocalSearch.random_init(d_matrix, f_matrix)
        best_solution = current_solution.get_copy()
        p_matrix = GuidedLocalSearch.init_p_matrix(d_matrix.get_max_row_index() + 1)

        t = time.time()
        while time.time() - t < 30 * 60:
            current_solution = GuidedLocalSearch.local_search_with_penalty(current_solution, p_matrix)
            GuidedLocalSearch.update_penalty_matrix(current_solution, d_matrix, f_matrix, p_matrix)

            if BaseLocalSearch.accept(best_solution.get_cost(), current_solution.get_cost()):
                best_solution = current_solution.get_copy()

        return best_solution

    @classmethod
    def local_search_with_penalty(cls, current_solution, p_matrix):
        dimension = len(current_solution.get_bijection_map())
        best_cost = current_solution.get_cost_with_penalty(p_matrix, cls.lambda_weight)

        while True:
            was_improved = False
            for i in range(0, dimension - 1):
                for j in range(i + 1, dimension):
                    new_solution = Solution.swap(current_solution, i, j)
                    new_cost = new_solution.get_cost_with_penalty(p_matrix, cls.lambda_weight)
                    if new_cost < best_cost:
                        best_cost = new_cost
                        current_solution = new_solution.get_copy()
                        was_improved = True

            if not was_improved:
                break

        return current_solution

    @classmethod
    def init_p_matrix(cls, dimension):
        p_matrix = Matrix()
        for i in range(0, dimension):
            for j in range(0, dimension):
                p_matrix.set_coordinate(Coordinate(i, j), 0)

        return p_matrix

    @classmethod
    def update_penalty_matrix(cls, current_solution, d_matrix, f_matrix, p_matrix):
        bijection_map = current_solution.get_bijection_map()
        dimension = p_matrix.get_max_row_index() + 1
        max_utility_index = 0
        max_utility_value = 0
        for i in range(0, dimension):
            cost = 0
            for j in range(0, dimension):
                cost += d_matrix.get_value_by_coordinate(i, j) * f_matrix.get_value_by_coordinate(
                    bijection_map[i], bijection_map[j])
            current_utility_value = cost / (1 + p_matrix.get_value_by_coordinate(i, bijection_map[i]))
            if current_utility_value > max_utility_value:
                max_utility_value = current_utility_value
                max_utility_index = i

        p_matrix.set_coordinate(Coordinate(max_utility_index, bijection_map[max_utility_index]),
                                p_matrix.get_value_by_coordinate(
                                    max_utility_index, bijection_map[max_utility_index]) + 1)
