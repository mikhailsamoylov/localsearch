import re
from Matrix import Matrix
from Coordinate import Coordinate


class Parser:
    @staticmethod
    def get_distances_matrix(filename):
        matrix = Matrix()
        f = open(filename)
        pattern = "([\\d]+)"

        temp = f.readline()
        result = re.findall(pattern, temp)
        if len(result) == 1:
            vertices_count = int(result[0])
        else:
            raise Exception("invalid file format")

        temp = f.readline()

        row_pointer = 0
        column_pointer = 0

        while row_pointer < vertices_count:
            result = re.findall(pattern, temp)

            for i in result:
                matrix.set_coordinate(Coordinate(row_pointer, column_pointer), int(i))
                column_pointer += 1
            row_pointer += 1
            column_pointer = 0
            temp = f.readline()

        return matrix

    @staticmethod
    def get_flows_matrix(filename):
        matrix = Matrix()
        f = open(filename)
        pattern = "([\\d]+)"

        temp = f.readline()
        result = re.findall(pattern, temp)
        if len(result) == 1:
            vertices_count = int(result[0])
        else:
            raise Exception("invalid file format")

        for i in range(0, vertices_count + 1):
            f.readline()

        temp = f.readline()

        row_pointer = 0
        column_pointer = 0

        while row_pointer < vertices_count:
            result = re.findall(pattern, temp)

            for i in result:
                matrix.set_coordinate(Coordinate(row_pointer, column_pointer), int(i))
                column_pointer += 1
            row_pointer += 1
            column_pointer = 0
            temp = f.readline()

        return matrix
