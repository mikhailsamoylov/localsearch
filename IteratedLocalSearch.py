import time
import random

from BaseLocalSearch import BaseLocalSearch


class IteratedLocalSearch (BaseLocalSearch):

    @classmethod
    def search(cls, d_matrix, f_matrix):
        current_solution = BaseLocalSearch.random_init(d_matrix, f_matrix)
        best_solution = BaseLocalSearch.local_search(current_solution)
        current_solution = best_solution.get_copy()
        t = time.time()
        while time.time() - t < 30 * 60:
            current_solution = IteratedLocalSearch.perturbate(current_solution)
            current_solution = BaseLocalSearch.local_search(current_solution)
            if BaseLocalSearch.accept(best_solution.get_cost(), current_solution.get_cost()):
                best_solution = current_solution.get_copy()

        return best_solution

    @classmethod
    def perturbate(cls, current_solution):
        current_solution = current_solution.get_copy()
        bijection_map = current_solution.get_bijection_map()
        dimension = len(bijection_map) - 1
        for i in range(0, 2):
            x = random.randint(0, dimension)
            y = random.randint(0, dimension)
            temp = bijection_map[x]
            bijection_map[x] = bijection_map[y]
            bijection_map[y] = temp

        current_solution.set_bijection_map(bijection_map)
        return current_solution
