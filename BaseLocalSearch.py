import random

from Solution import Solution


class BaseLocalSearch:

    @classmethod
    def random_init(cls, d_matrix, f_matrix):
        bijection_map = list(range(0, d_matrix.get_max_row_index() + 1))
        random.shuffle(bijection_map)
        return Solution(d_matrix, f_matrix, bijection_map)

    @classmethod
    def local_search(cls, current_solution):
        dimension = len(current_solution.get_bijection_map())
        best_cost = current_solution.get_cost()

        while True:
            was_improved = False
            for i in range(0, dimension - 1):
                for j in range(i + 1, dimension):
                    new_solution = Solution.swap(current_solution, i, j)
                    new_cost = new_solution.get_cost()
                    if new_cost < best_cost:
                        best_cost = new_cost
                        current_solution = new_solution.get_copy()
                        was_improved = True

            if not was_improved:
                break

        return current_solution

    @classmethod
    def accept(cls, best_cost, current_cost):
        return best_cost > current_cost

    @classmethod
    def search(cls, d_matrix, f_matrix):
        raise Exception("You can't use search() method from this class directly")
