import time
from BaseLocalSearch import BaseLocalSearch


class RepeatedLocalSearch (BaseLocalSearch):

    @classmethod
    def search(cls, d_matrix, f_matrix):
        current_solution = BaseLocalSearch.random_init(d_matrix, f_matrix)
        best_solution = BaseLocalSearch.local_search(current_solution)
        t = time.time()
        while time.time() - t < 30 * 60:
            current_solution = BaseLocalSearch.random_init(d_matrix, f_matrix)
            current_solution = BaseLocalSearch.local_search(current_solution)
            if BaseLocalSearch.accept(best_solution.get_cost(), current_solution.get_cost()):
                best_solution = current_solution.get_copy()

        return best_solution
