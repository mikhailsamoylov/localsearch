class Coordinate:
    def __init__(self, row, column):
        self.__row = row
        self.__column = column

    def __eq__(self, other):
        if type(self) != type(other):
            return False
        return self.__row == other.get_row_index() and self.__column == other.get_column_index()

    def __hash__(self):
        result = 17
        result = 31 * result + self.__row
        result = 31 * result + self.__column
        return result

    def get_row_index(self):
        return self.__row

    def get_column_index(self):
        return self.__column
