import sys
from Lib import random
from Coordinate import Coordinate


class Matrix:
    def __init__(self):
        self.__matrix = {}
        self.__max_columns = {}
        self.__min_columns = {}
        self.__min_row = sys.maxsize
        self.__max_row = 0

    def get_value_by_coordinate(self, row, column):
        coordinate = Coordinate(row, column)
        if coordinate in self.__matrix:
            value = self.__matrix[coordinate]
        else:
            raise Exception("out of bounds, man")

        return value

    def set_coordinate(self, c, value):
        self.__matrix[c] = value
        self.__set_bounds(c.get_row_index(), c.get_column_index())

    def __set_bounds(self, row_index, column_index):
        if row_index > self.__max_row:
            self.__max_row = row_index

        current_max_row = None
        if row_index in self.__max_columns:
            current_max_row = self.__max_columns[row_index]

        if current_max_row is None or column_index > current_max_row:
            self.__max_columns[row_index] = column_index

        if row_index < self.__min_row:
            self.__min_row = row_index

        current_min_row = None
        if row_index in self.__min_columns:
            current_min_row = self.__min_columns[row_index]

        if current_min_row is None or column_index < current_min_row:
            self.__min_columns[row_index] = column_index

    def get_max_row_index(self):
        return self.__max_row

    def get_max_column_index(self, row_index):
        return self.__max_columns[row_index] if row_index in self.__max_columns else 0

    def get_min_row_index(self):
        return self.__min_row

    def get_min_column_index(self, row_index):
        return self.__min_columns[row_index] if row_index in self.__min_columns else 0

    def print_me(self):
        for i in range(self.get_min_row_index(), self.get_max_row_index() + 1):
            for j in range(self.get_min_column_index(i), self.get_max_column_index(i) + 1):
                print(self.get_value_by_coordinate(i, j), end=" ")
            print()
        print()

    def get_random_coordinate(self):
        random_number = random.randint(0, len(self.__matrix))
        current_pos = 0
        x = 0
        y = 0

        while True:
            max_column_index = self.get_max_column_index(x)
            if current_pos + max_column_index >= random_number:
                y = random_number - current_pos
            else:
                x += 1
            current_pos += max_column_index + 1
            if current_pos > random_number:
                break

        return Coordinate(x, y)

    def get_element_numbers(self):
        count = 0
        for i in range(self.__min_row, self.__max_row):
            count += self.get_max_column_index(i)

        return count

    def delete_coordinate(self, coordinate):
        row_index = coordinate.get_row_index()
        self.__max_columns[row_index] = self.get_max_column_index(row_index) - 1
        del self.__matrix[coordinate]
