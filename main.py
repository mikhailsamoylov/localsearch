from Parser import Parser
from RepeatedLocalSearch import RepeatedLocalSearch
from IteratedLocalSearch import IteratedLocalSearch
from GuidedLocalSearch import GuidedLocalSearch

instance_dir = "test_instances/"
instance_file = "tai100a"
instance_path = instance_dir + instance_file

solution_dir = "solutions/"
solution_file = instance_file + ".sol"

d_matrix = Parser.get_distances_matrix(instance_path)
f_matrix = Parser.get_flows_matrix(instance_path)

solution = RepeatedLocalSearch.search(d_matrix, f_matrix)
solution.print_solution()
solution.save_solution(solution_dir + "rls/" + solution_file)

solution = IteratedLocalSearch.search(d_matrix, f_matrix)
solution.print_solution()
solution.save_solution(solution_dir + "ils/" + solution_file)

solution = GuidedLocalSearch.search(d_matrix, f_matrix)
solution.print_solution()
solution.save_solution(solution_dir + "gls/" + solution_file)
